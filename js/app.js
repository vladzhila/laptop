// SOFT param { name, os, cpu, ram, videoCard, hdd }

var gta = new Soft('GTA', 'windows', 7, 0.5, 2, 60);
var chrome = new Soft('chrome', 'windows', 3, 0.4, 0.512, 1);
var nfs = new Soft('NFS', 'windows', 3, 0.1, 0.512, 2);
var dota = new Soft('Dota 2', 'linux', 3, 0.1, 0.512, 2);
var photoshop = new Soft('Photoshop CS6', 'macintosh', 3, 0.5, 1, 3);
var cs = new Soft('CS 1.6', 'windows', 3, 0.2, 1, 2);

// LAPTOPS param { name, os, cpu, videoCard, ram, hdd

var asusK56 = new Laptop('Asus K56', 'windows', 7, 2, 16, 1000);
var macBookPro15 = new Laptop('MacBook Pro 15', 'macintosh', 7, 2, 16, 1000);
var hp200 = new Laptop('HP 200', 'windows', 3, 1, 4, 500);

// USERS param { name, laptop }

var vasya = new User('Vasya', hp200);
