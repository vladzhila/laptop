function OperatingSystem(type, name) {

  this.type = type;

  // each OS uses different amount memory(RAM and HDD) in laptop
  switch (type) {
    case 'linux':
      this.ram = 0.8;
      this.hdd = 15;
      break;
    case 'windows':
      this.ram = 1;
      this.hdd = 25;
      break;
    case 'macintosh':
      this.ram = 0.9;
      this.hdd = 20;
      break;
    default:
      throw 'Not working Operating System ' + '(' + name + ')';
      break;
  }

}