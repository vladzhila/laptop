function Laptop(name, os, cpu, videoCard, ram, hdd) {

  var battery, hardware, soft, operatingSystem, enabled;

  battery         = new Battery(name, 100);
  hardware        = new Hardware(name, cpu, videoCard, ram, hdd);
  soft            = new Soft();
  operatingSystem = new OperatingSystem(os, name);

  // use memory(HDD) of operating system in laptop
  hardware.hdd -= operatingSystem.hdd;
  enabled = false;

  /**
   * enable laptop and his discharge and pick up memory(RAM) for operation system
   * @see {@link battery}
   * @see {@link hardware}
   * @see {@link operatingSystem}
   */
  this.enable = function() {
    enabled = true;
    battery.enable();

    hardware.ram -= operatingSystem.ram;
  };

  /**
   * disable all programs then disable laptop and stop discharge battery
   * then return memory(RAM) in laptop
   * @see {@link soft}
   * @see {@link battery}
   * @see {@link hardware}
   * @see {@link operatingSystem}
   */
  this.disable = function() {

    for (var i = 0; i < soft.allSoft.length; i++) {
      if (soft.allSoft[i].statusEnabled()) {
        this.switchOffSoft(soft.allSoft[i]);
      }
    }

    enabled = false;
    battery.disable();

    hardware.ram += operatingSystem.ram;
  };

  /**
   * call method battery's for inclusion charge laptop
   * @see {@link battery}
   */
  this.enableCharge = function() {
    battery.enableCharge();
  };

  /**
   * call method battery's for shutdown charge laptop
   * @see {@link battery}
   */
  this.disableCharge = function() {
    battery.disableCharge();
  };

  /**
   * when laptop enabled and program preset in laptop
   * and all characteristics support then installing program
   * and use memory(HDD) in laptop
   * @param {object} program - soft that we want to install to laptop
   * @see {@link hardware}
   * @see {@link operatingSystem}
   * @see {@link soft}
   */
  this.installSoft = function(program) {

    if (enabled) {
      if (program) {
        if (hardware.hdd > program.hdd) {
          if (hardware.cpu >= program.cpu) {
            if (hardware.videoCard >= program.videoCard) {
              if (program.os == operatingSystem.type) {
                soft.install(program);
                hardware.takeHdd(program.hdd);
              } else {
                throw program.name + ' no support your operating system';
              }
            } else {
              throw program.name + ' need better video card';
            }
          } else {
            throw program.name + ' need better CPU';
          }
        } else {
          throw program.name + ' low memory(HDD)';
        }
      } else {
        throw 'Please, enter a program';
      }
    } else {
      throw 'Please, enable laptop';
    }
  };

  /**
   * when laptop enabled then call method soft's for removing program from laptop
   * and return memory(HDD) in laptop
   * @param program - soft that we want to remove from laptop
   * @see {@link soft}
   * @see {@link hardware}
   */
  this.removeSoft = function(program) {

    if (enabled) {
      soft.remove(program);
      hardware.returnHdd(program.hdd);
    } else {
      throw 'Please, enable laptop';
    }
  };

  /**
   * when laptop enabled and characteristics support then launch program
   * and use memory(RAM) in laptop
   * when not enough memory(RAM) then disable laptop
   * @param {object} program - soft that we want to launch
   * @see {@link hardware}
   * @see {@link soft}
   * @this Laptop
   */
  this.launchSoft = function(program) {

    if (enabled) {
      if (hardware.cpu >= program.cpu) {
        if (hardware.videoCard >= program.videoCard) {
          soft.launch(program);
          hardware.takeRam(program.ram);

          if (hardware.ram <= 0) {
            this.disable();
            throw program.name + ' memory(RAM) overload, laptop disabled';
          }
        } else {
          throw program.name + ' need better video card';
        }
      } else {
        throw program.name + ' need better CPU';
      }
    } else {
      throw 'Please, enable laptop';
    }
  };

  /**
   * when laptop enabled then call method soft's for shutdown program
   * and return memory(RAM) in laptop
   * @param {object} program - soft that we want to switch off
   * @see {@link soft}
   * @see {@link hardware}
   */
  this.switchOffSoft = function(program) {

    if (enabled) {
        soft.switchOff(program);
        hardware.returnRam(program.ram);
    } else {
      throw 'Laptop disable';
    }
  };

  /**
   * change CPU laptop when laptop disabled and new CPU
   * support only (i3, i5, i7) (code: 3, 5, 7)
   * @param {number} newCpu - new CPU that we want to put
   * @see {@link hardware}
   */
  this.changeCpu = function(newCpu) {

    if (!enabled) {
      if (newCpu == 3 || newCpu == 5 || newCpu == 7) {
        cpu = newCpu;
        hardware.cpu = newCpu;
      } else {
        throw 'Do not support this CPU';
      }
    } else {
      throw 'Please, disable laptop before change CPU';
    }
  };

  /**
   * change video card laptop when laptop disabled and new video card
   * support only 0.512GB or 1GB or 2GB (code: 0.512, 1, 2)
   * @param {number} newVideoCard - new video card that we want to put
   * @see {@link hardware}
   */
  this.changeVideoCard = function(newVideoCard) {

    if (!enabled) {
      if (newVideoCard == 0.512 || newVideoCard == 1 || newVideoCard == 2) {
        videoCard = newVideoCard;
        hardware.videoCard = newVideoCard;
      } else {
        throw 'Do not support this video card';
      }
    } else {
      throw 'Please, disable laptop before change video card';
    }
  };

  /**
   * add or remove memory(RAM) when laptop disabled and amount support
   * and also range from 0 to 16 GB
   * @param {number} amount - new amount memory(RAM) that we want to add or remove
   * when we want to add amount memory(RAM) use (code: 2, 4, 8, 16)
   * when we want to remove amount memory(RAM) use (code: -2, -4, -8, -16)
   */
  this.changeAmountRam = function(amount) {

    if (!enabled) {
      var variantChangeRam = [-16, -8, -4, -2, 2, 4, 8, 16];

      if (~variantChangeRam.indexOf(amount)) {
        var fullRam = hardware.ram + amount;

        if (fullRam <= 16 && fullRam > 0) {
          hardware.ram += amount;
          ram += amount;
        } else {
          throw 'Unacceptable value memory (ram)';
        }
      } else {
        throw 'Unacceptable value memory (ram)';
      }
    } else {
      throw 'Please, disable laptop before add memory(ram)';
    }
  };

  /**
   * change amount memory(HDD) in laptop and subtract memory(HDD) of operation system
   * when laptop disabled and support amount memory(HDD)
   * @param {number} amount - new amount memory(HDD) that we want to put
   * @see {@link hardware}
   * @see {@link operatingSystem}
   */
  this.changeAmountHdd = function(amount) {

    if (!enabled) {
      if (amount == 1000 || amount == 500) {
        hardware.hdd = amount;
        hdd = amount;
        hardware.hdd -= operatingSystem.hdd;
      } else {
        throw 'Do not support this memory(hdd)';
      }
    } else {
      throw 'Please, disable laptop before change memory(hdd)';
    }
  };

  /**
   * when laptop enabled and laptop support new operating operatingSystem
   * then remove all soft also return old memory(HDD) operating system
   * and then put new operating system with new amount memory(HDD)
   * and disable laptop
   * @param {string} newOs - new operating system that we want to put
   * change type operation system on ('windows', 'linux', 'macintosh')
   * @see {@link hardware}
   * @see {@link operatingSystem}
   * @see {@link soft}
   * @this Laptop
   */
  this.changeOs = function(newOs) {

    if (enabled) {
      if (newOs == 'windows' || newOs == 'linux' || newOs == 'macintosh') {

        while (soft.allSoft.length) {
          var i = 0;
          this.removeSoft(soft.allSoft[i]);
        }

        hardware.hdd += operatingSystem.hdd;

        this.disable();

        operatingSystem = new OperatingSystem(newOs, name);
        hardware.hdd -= operatingSystem.hdd;
      } else {
        throw 'Do not support this OS';
      }
    } else {
      throw 'Please, enable laptop before change OS';
    }
  };

  /**
   * call method soft's for return all name soft that installed in laptop
   * @returns {string} - all soft that installed
   * @see {@link soft}
   */
  this.getSoft = function() {
    return soft.getSoft();
  };

  /**
   * create object laptop in which described all properties
   * about characteristics hardware of laptop
   * @returns {string} - info about all hardware in laptop
   * @see {@link operatingSystem}
   * @see {@link hardware}
   */
  this.getCharacteristics = function() {

    var laptop = {
      'Name': name,
      'Operating System': operatingSystem.type,
      'CPU': hardware.cpu,
      'Video Card': hardware.videoCard,
      'RAM': ram + ' : ' + hardware.ram + ' free',
      'HDD': hdd + ' : ' + hardware.hdd + ' free'
    };

    for (var prop in laptop) {
      console.log( prop + ' : ' + laptop[prop] );
    }
  };

  /**
   * call method battery's that know present amount charge in laptop
   * @returns {number} - percent that remained (1-100%)
   * @see {@link battery}
   */
  this.getAmountCharge = function() {
    return battery.getAmountCharge();
  };

  /**
   * call method battery's that check disabled or enabled laptop
   * @returns {boolean} - enabled(true) of disabled(false)
   * @see {@link battery}
   */
  this.checkedEnabled = function() {
    return battery.checkedEnabled();
  };
}