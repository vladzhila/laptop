function Hardware(nameLaptop, cpu, videoCard, ram, hdd) {

  this.cpu = cpu;
  this.videoCard = videoCard;
  this.ram = ram;
  this.hdd = hdd;

  // CPU - intel i3,i5,i7 (code: 3, 5, 7)
  if (cpu == 3 || cpu == 5 || cpu == 7) {
    this.cpu = cpu;
  } else {
    throw nameLaptop + ' broken CPU';
  }

  // RAM - 2GB, 4GB, 8GB, 16GB (code: 2, 4, 8, 16)
  if (ram == 2 || ram == 4 || ram == 8 || ram == 16) {
    this.ram = ram;
  } else {
    throw nameLaptop + ' broken RAM';
  }

  // HDD - 500GB, 1000GB (code: 500, 1000)
  if (hdd == 500 || hdd == 1000) {
    this.hdd = hdd;
  } else {
    throw nameLaptop + ' broken HDD';
  }

  // video card - 0.512GB or 1GB or 2GB (code: 0.512, 1, 2)
  if (videoCard == 0.512 || videoCard == 1 || videoCard == 2) {
    this.videoCard = videoCard;
  } else {
    throw nameLaptop + ' broken video card';
  }

  /**
   * use amount memory(RAM) in laptop
   * @param {number} amount - memory(RAM) that we want to use in laptop
   * @this Hardware
   */
  this.takeRam = function(amount) {
    this.ram -= amount;
  };

  /**
   * return amount memory(RAM) in laptop
   * @param amount - memory(RAM) that we want to return in laptop
   * @this Hardware
   */
  this.returnRam = function(amount) {
    this.ram += amount;
  };

  /**
   * use amount memory(HDD)
   */

  /**
   * use amount memory(HDD) in laptop
   * @param amount - memory(HDD) that we want to use in laptop
   * @this Hardware
   */
  this.takeHdd = function(amount) {
    this.hdd -= amount;
  };

  /**
   * return amount memory(HDD) in laptop
   * @param amount - memory(HDD) that we want to return in laptop
   * @this Hardware
   */
  this.returnHdd = function(amount) {
    this.hdd += amount;
  };
}