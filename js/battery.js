function Battery(nameLaptop, percentCharge) {
  var increase, reduce;

  if (typeof percentCharge == 'number' && percentCharge <= 100 && percentCharge >= 0) {
    amountCharge = percentCharge; // amount charge in percent
  } else {
    throw nameLaptop + ' broken battery';
  }

  var enabled = false;
  var charging = false;

  /**
   * enable laptop and call func for reduce amount charge
   */
  this.enable = function() {
    enabled = true;

    reduceCharge();
  };

  this.disable = function() {
    enabled = false;
  };

  /**
   * enable charging and call func for increase charge
   */
  this.enableCharge = function() {
    charging = true;

    increaseCharge();
  };

  /**
   * disable charging if it's disable and call func for reduce charge
   */
  this.disableCharge = function() {
    if (charging) {
      charging = false;

      reduceCharge();
    } else if (enabled) {
      throw 'charging disable';
    }
  };

  /**
   * return amount remaining charge (%)
   * @returns {number} - percent that remained (1-100%)
   */
  this.getAmountCharge = function() {
    return amountCharge;
  };

  /**
   * check on enabled or disabled laptop
   * @returns {boolean} - enabled or disabled laptop
   */
  this.checkedEnabled = function() {
    return enabled;
  };

  function reduceCharge(){
    reduce = setInterval(function() {

      // if laptop enabled and without charging than reduce charge, when battery 0 disable laptop
      if (enabled && !charging) {
        if (amountCharge != 0) {
          amountCharge--;
          console.log( amountCharge );
        } else {
          enabled = false;
        }
      } else {
        clearInterval(reduce);
      }
    }, 2000);
  }

  function increaseCharge() {
    increase = setInterval(function() {

      // if is charging and amount charge less than 100 then increase charge
      if (charging) {
        if (amountCharge != 100) {
          amountCharge += 1;
          console.log( amountCharge );
        } else if (amountCharge == 100) {
          clearInterval(reduce);
          clearInterval(increaseCharge);
        }
      } else {
        clearInterval(increaseCharge);
      }
    }, 1000);
  }
}