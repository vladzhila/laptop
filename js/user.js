function User(name, laptop) {

  /**
   * call method laptop's for inclusion laptop
   */
  this.enableLaptop = function() {
    laptop.enable();
  };

  /**
   * call method laptop's for shutdown laptop
   */
  this.disableLaptop = function() {
    laptop.disable();
  };

  /**
   * call method laptop's for inclusion charge laptop
   */
  this.enableCharge = function() {
    laptop.enableCharge()
  };

  /**
   * call method laptop's for shutdown charge laptop
   */
  this.disableCharge = function() {
    laptop.disableCharge();
  };

  /**
   * call method laptop's for installation program to laptop
   * @param {object} program - soft which we want to install to laptop
   */
  this.installSoft = function(program) {
    laptop.installSoft(program);
  };

  /**
   * call method laptop's for removing program from laptop
   * @param {object} program - soft which we want to remove from laptop
   */
  this.removeSoft = function(program) {
    laptop.removeSoft(program);
  };

  /**
   * call method laptop's for launching program
   * @param {object} program - soft which we want to launch
   */
  this.launchSoft = function(program) {
    laptop.launchSoft(program);
  };

  /**
   * call method laptop's for shutdown program
   * @param {object} program - soft which we want to switch off
   */
  this.switchOffSoft = function(program) {
    laptop.switchOffSoft(program);
  };

  /**
   * call method laptop's for changing CPU laptop's
   * if our laptop have bad CPU for programs we can to change it
   * @param {number} newCpu - new CPU that we want to put
   * change CPU on (i3, i5, i7) (code: 3, 5, 7)
   */
  this.changeCpu = function(newCpu) {
    laptop.changeCpu(newCpu);
  };

  /**
   * call method laptop's for changing video card laptop's
   * if our laptop have bad video card for programs we can to change it
   * @param {number} newVideoCard - new video card that we want to put
   * change video card on 0.512GB or 1GB or 2GB (code: 0.512, 1, 2)
   */
  this.changeVideoCard = function(newVideoCard) {
    laptop.changeVideoCard(newVideoCard);
  };

  /**
   * call method laptop's for changing amount memory(RAM) laptop's
   * if our laptop have little amount memory(RAM) for programs, we can to add it
   * @param {number} amount - new amount memory(RAM) that we want to add or remove
   * when we want to add amount memory(RAM) (code: 2, 4, 8, 16)
   * when we want to remove amount memory(RAM) (code: -2, -4, -8, -16)
   */
  this.changeAmountRam = function(amount) {
    laptop.changeAmountRam(amount);
  };

  /**
   * call method laptop's for changing amount memory(HDD) laptop's
   * if our laptop have little amount memory(HDD) for programs, we can to add it
   * @param {number} amount - new amount memory(HDD) that we want to put
   * change amount memory(HDD) on (500GB, 1000GB) (code: 500, 1000)
   */
  this.changeAmountHdd = function(amount) {
    laptop.changeAmountHdd(amount);
  };

  /**
   * call method laptop's for changing type operation system laptop's
   * if our laptop not support of programs that we want to install, we can to change it
   * @param {string} newOs - new operating system that we want to put
   * change type operation system on ('windows', 'linux', 'macintosh')
   */
  this.changeOs = function(newOs) {
    laptop.changeOs(newOs);
  };

  /**
   * if we want to change user laptop's for work
   * we check that our laptop it was not the same how new laptop
   * then disable laptop and charge and change our laptop
   * @param {object} newLaptop - new laptop that we want to change at user
   * change laptop (all objects of laptop that we created)
   */
  this.changeLaptop = function(newLaptop) {
    if (laptop != newLaptop) {
      laptop.disable();
      laptop.disableCharge();
      laptop = newLaptop;
    } else {
      throw 'You use this laptop';
    }
  };

  /**
   * call method laptop's for return all name soft that installed in laptop
   * @returns {string} - all soft that installed
   */
  this.getSoft = function() {
    return laptop.getSoft();
  };

  /**
   * call method laptop's for return all characteristics laptop's
   * @returns {string} - info about all hardware in laptop
   */
  this.getCharacteristicsLaptop = function() {
    return laptop.getCharacteristics();
  };

  /**
   * call method laptop's that know present amount charge in laptop
   * @returns {number} - percent that remained (1-100%)
   */
  this.getAmountCharge = function() {
    return laptop.getAmountCharge();
  };

  /**
   * call method laptop's that check disabled or enabled laptop
   * @returns {boolean} - enabled(true) of disabled(false)
   */
  this.checkedEnabled = function() {
    return laptop.checkedEnabled();
  };
}