function Soft(name, os, cpu, ram, videoCard, hdd) {

  this.name = name;
  this.os = os;
  this.cpu = cpu;
  this.ram = ram;
  this.videoCard = videoCard;
  this.hdd = hdd;

  var enabled = false;
  var soft = [];

  this.allSoft = soft;

  /**
   * create empty array in which add name of programs
   * that installed in laptop
   * @returns {string} - all name program that installed in laptop
   */
  this.getSoft = function() {
    var cloneSoft = [];

    for (var i = 0; i < soft.length; i++) {
      cloneSoft.push(soft[i].name);
    }

    return cloneSoft.join(', ');
  };

  /**
   *  if in array soft there ins't program which we want to install
   *  that add to array soft this program
   * @param {object} program - soft which we want to install
   */
  this.install = function(program) {

    if (!~soft.indexOf(program)) {
      soft.push(program);
    } else {
      throw program.name + ' already install';
    }
  };

  /**
   * if program disabled and present then
   * check it in array soft, if found then remove
   * @param {object} program - soft which we want to remove
   */
  this.remove = function(program) {

    if (!program.statusEnabled()) {
      if (program) {
        if (~soft.indexOf(program)) {
          soft.splice(soft.indexOf(program), 1);
        } else {
          throw program.name + ' is not installed';
        }
      } else {
        throw 'Please, enter a program';
      }
    } else {
      throw 'First of all switch off ' + program.name;
    }
  };

  /**
   * when program present, disabled and found in
   * array soft then launch program
   * @param {object} program - soft which we want to launch
   */
  this.launch = function(program) {
    if (program) {
      if (!program.statusEnabled()) {
        var checkProgram = soft.indexOf(program);

        if (~checkProgram) {
          program.statusEnabled.call(this, true);
          console.log( soft[checkProgram].name + ' is launched' );
        } else {
          throw program.name + ' is not installed';
        }
      } else {
        throw program.name + ' already launched';
      }
    } else {
      throw 'Please, enter a program';
    }
  };

  /**
   * when program present, enabled and found in
   * array soft then switch off program
   * @param {object} program - soft which we want to disable
   */
  this.switchOff = function(program) {
    if (program) {
      if (program.statusEnabled()) {
        var checkProgram = soft.indexOf(program);

        if (~checkProgram) {
          program.statusEnabled.call(this, false);
          console.log( soft[checkProgram].name + ' is disabled' );
        } else {
          throw program.name + ' is not installed';
        }
      } else {
        throw program.name + ' is disable';
      }
    } else {
      throw 'please, enter a program';
    }
  };

  /**
   * if is param present then change status, if not then return status laptop
   * @param status - enabled(true) or disabled(false)
   * @returns {boolean} - enabled(true) of disabled(false)
   */
  this.statusEnabled = function(status) {

    if (arguments.length) {
      enabled = status;
    }

    return enabled;
  }
}